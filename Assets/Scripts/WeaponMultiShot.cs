using UnityEngine;
using System.Collections;

public class WeaponMultiShot : WeaponSuperclass {

	[Range(0,360f)]
	public float spreadAngle = 20f;

	public int numberOfProjectiles = 3;
	public Sprite projectileSprite;
	public float projectileScale = 0.5f;


	private GameObject[] _projectiles;
	private GameManager _gameManager;

	void Start()
	{
		_gameManager = GameManager.instance;

		_projectiles = new GameObject[ numberOfProjectiles ];

		float tHalfSpreadAngle = spreadAngle * 0.5f;
		float tLeftFacingAngles = tHalfSpreadAngle;
		float tIncrementalAngles = spreadAngle / numberOfProjectiles;
		float tRotationChangeAngles;

		float tX, tY;
		float tRelativeAngleRads;
		int i;
		GameObject tProjectile;
		for( i = 0; i < numberOfProjectiles; i++ )
		{
			tProjectile = new GameObject();
			tProjectile.transform.parent = transform; 
			// when using Quaternion.Euler it seems that positive numbers rotate counter-clockwise around z, 
			// and vice-versa for negative numbers
			tRotationChangeAngles = tLeftFacingAngles - ( tIncrementalAngles * i );
			tProjectile.transform.rotation = Quaternion.Euler( 0, 0, tRotationChangeAngles ) * transform.rotation;
			tProjectile.transform.position = transform.position;
			var tProjectileSpriteRenderer = tProjectile.AddComponent<SpriteRenderer>();
			tProjectileSpriteRenderer.sprite = projectileSprite;
			var tProjectileBoxCollider2d = tProjectile.AddComponent<BoxCollider2D>();
			tProjectile.transform.localScale = new Vector3( projectileScale, projectileScale, 1f );
			tProjectileBoxCollider2d.size = projectileSprite.bounds.size; 

			_projectiles[ i ] = tProjectile;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		GameObject tProjectile;
		int i;
		var tOneOrMoreInBounds = false;
		for( i = 0; i < numberOfProjectiles; i++ )
		{
			tProjectile = _projectiles[ i ];
			tProjectile.transform.position += tProjectile.transform.rotation * Vector3.right * projectileSpeed;

			//Make sure the bullet is still in the bounds of the game.
			//if any of the bullets aren't in bounds, delete all.
			if( _gameManager.IsInBounds( tProjectile.transform.position ) ) 
			{
				tOneOrMoreInBounds = true;
			}
		}

		if( !tOneOrMoreInBounds )
		{
				Destroy( this.gameObject );
		}
	}

}
