using UnityEngine;
using System.Collections;

public class WeaponTripleShot : WeaponSuperclass {
	private GameObject _projectileLeft, _projectileMiddle, _projectileRight;

	[Range(0,360f)]
	public float spreadAngle = 20f;

	public Sprite projectileLeftSprite;
	public float projectileLeftScale = 0.5f;
	public Sprite projectileMiddleSprite;
	public float projectileMiddleScale = 0.5f;
	public Sprite projectileRightSprite;
	public float projectileRightScale = 0.5f;
	
	void Start()
	{
		float tRotationChangeRads;
		float tRotationChangeAngles;

		float tHalfSpreadAngle = spreadAngle * 0.5f; 
		float tX, tY;
		float tAngleFacingRads;

		_projectileLeft = new GameObject();
		_projectileLeft.transform.parent = transform; 
		tRotationChangeAngles = tHalfSpreadAngle;
		_projectileLeft.transform.rotation = Quaternion.Euler( 0, 0, tRotationChangeAngles ) * transform.rotation;
		_projectileLeft.transform.position = transform.position;
		var tProjectileLeftSpriteRenderer = _projectileLeft.AddComponent<SpriteRenderer>();
		tProjectileLeftSpriteRenderer.sprite = projectileLeftSprite;
		var tProjectileLeftBoxCollider2d = _projectileLeft.AddComponent<BoxCollider2D>();
		_projectileLeft.transform.localScale = new Vector3( projectileLeftScale, projectileLeftScale, 1f );
		tProjectileLeftBoxCollider2d.size = projectileLeftSprite.bounds.size; 

		_projectileMiddle = new GameObject();
		_projectileMiddle.transform.parent = transform; 
		tRotationChangeAngles = 0;
		_projectileMiddle.transform.rotation = Quaternion.Euler( 0, 0, tRotationChangeAngles ) * transform.rotation;
		_projectileMiddle.transform.position = transform.position;
		var tProjectileMiddleSpriteRenderer = _projectileMiddle.AddComponent<SpriteRenderer>();
		tProjectileMiddleSpriteRenderer.sprite = projectileMiddleSprite;
		var tProjectileMiddleBoxCollider2d = _projectileMiddle.AddComponent<BoxCollider2D>();
		_projectileMiddle.transform.localScale = new Vector3( projectileMiddleScale, projectileMiddleScale, 1f );
		tProjectileMiddleBoxCollider2d.size = projectileMiddleSprite.bounds.size; 

		_projectileRight = new GameObject();
		_projectileRight.transform.parent = transform; 
		tRotationChangeRads = ( _projectileRight.transform.eulerAngles.z - tHalfSpreadAngle ) * Mathf.Deg2Rad;
		_projectileRight.transform.rotation = transform.rotation * Quaternion.AngleAxis( tRotationChangeRads, Vector3.back );
		tRotationChangeAngles = -tHalfSpreadAngle;
		_projectileRight.transform.rotation = Quaternion.Euler( 0, 0, tRotationChangeAngles ) * transform.rotation;
		_projectileRight.transform.position = transform.position;
		var tProjectileRightSpriteRenderer = _projectileRight.AddComponent<SpriteRenderer>();
		tProjectileRightSpriteRenderer.sprite = projectileRightSprite;
		var tProjectileRightBoxCollider2d = _projectileRight.AddComponent<BoxCollider2D>();
		_projectileRight.transform.localScale = new Vector3( projectileRightScale, projectileRightScale, 1f );
		tProjectileRightBoxCollider2d.size = projectileRightSprite.bounds.size; 
	}

	// Update is called once per frame
	void Update () {
		_projectileLeft.transform.position += _projectileLeft.transform.rotation * Vector3.right * projectileSpeed;
		_projectileMiddle.transform.position += _projectileMiddle.transform.rotation * Vector3.right * projectileSpeed;
		_projectileRight.transform.position += _projectileRight.transform.rotation * Vector3.right * projectileSpeed;

		//Make sure the bullet is still in the bounds of the game.
		bool inBounds = true;
		if( !GameManager.instance.IsInBounds(_projectileLeft.transform.position) || 
		   !GameManager.instance.IsInBounds(_projectileMiddle.transform.position) ||
		   !GameManager.instance.IsInBounds(_projectileRight.transform.position) )
		{
			inBounds = false;
		}

		//if it isn't, delete it.
		if(!inBounds) {
			Destroy(this.gameObject);
		}
	}

}
