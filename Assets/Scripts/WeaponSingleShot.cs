using UnityEngine;
using System.Collections;

public class WeaponSingleShot : WeaponSuperclass {
	public Sprite projectileSprite;
	public float projectileScale = 0.5f;

	private GameObject _projectile;

	void Start()
	{
		_projectile = new GameObject();
		_projectile.transform.parent = transform; 
		_projectile.transform.rotation = transform.rotation;
		_projectile.transform.position = transform.position;
		var tProjectileSpriteRenderer = _projectile.AddComponent<SpriteRenderer>();
		tProjectileSpriteRenderer.sprite = projectileSprite;
		var tProjectileBoxCollider2d = _projectile.AddComponent<BoxCollider2D>();
		_projectile.transform.localScale = new Vector3( projectileScale, projectileScale, 1f );
		tProjectileBoxCollider2d.size = projectileSprite.bounds.size; 
	}

	// Update is called once per frame
	void Update () {
		//The bullet will always move the direction it is facing, which was
		//	set when we spawned it in the Player class.
		//We move it forward by getting this forward vector(we'll consider the 
		//	character's forward to be to the right of the sprite), then adding it
		//	to the bullet's current position.
		Vector2 forward = _projectile.transform.right;
		forward.Normalize();
		Vector2 nextPosition = _projectile.transform.position;
		nextPosition.x += forward.x*projectileSpeed;
		nextPosition.y += forward.y*projectileSpeed;
		//Finally, we assign the position to our next position.
		_projectile.transform.position = nextPosition;

		//Check if the bullet is still in the bounds of the game.
		bool inBounds = GameManager.instance.IsInBounds(_projectile.transform.position);

		// if it has flown off the edge of the screen and we don't need it anymore
		// to save computer resources, delete it.
		if(!inBounds) {
			Destroy(this.gameObject);
		}
	}

}
