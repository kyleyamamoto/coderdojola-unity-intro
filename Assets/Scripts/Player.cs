using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//class variables:
	//	public variables are visible in the Unity inspector and additional classes
	//	put [HideInInspector] above a public variable if you don't want it to be visible
	//	private variables are hidden.
	public float playerRotationSpeed = 100f;
	public int playerHealth = 10;
	public GameObject weapon;
	public bool allowMovement = false;
	[Range(0,100)]
	public float movementSpeed = 20f;
	public bool continuousFiring = false;

	public KeyCode keyMoveLeft;
	public KeyCode keyMoveRight;
	public KeyCode keyMoveUp;
	public KeyCode keyMoveDown;
	public KeyCode keyRotateClockwise = KeyCode.RightArrow;
	public KeyCode keyRotateCounterClockwise = KeyCode.LeftArrow;
	public KeyCode keyFire = KeyCode.Space;

	private float _lastProjectileShotTime;
	private Text _livesDisplay;
	private SceneSwitcher _sceneSwitcher;
	private Canvas _canvas;
	private WeaponSuperclass _weaponScript;
	private AudioSource _audioInjury;
	private bool _isDying = false;
	private GameManager _gameManager;
	private Vector3 _velocity; 
	private Vector3 _acceleration;
	[Range(0, 1f)]
	private float _physicsDrag = 0.03f;

	// Use this for initialization
	void Start () {
		ResetBulletDelayTimer();

		_sceneSwitcher = GetComponent<SceneSwitcher>();

		_canvas = GameObject.Find("GameSceneCanvas").GetComponent<Canvas>();

		_livesDisplay = _canvas.transform.Find( "Lives Value" ).GetComponent<Text>(); 
		_livesDisplay.text = playerHealth.ToString();

		_weaponScript = weapon.GetComponent<WeaponSuperclass>();

		_audioInjury = GetComponent<AudioSource>();

		_gameManager = GameObject.Find( "GameManager" ).GetComponent<GameManager>();

		_acceleration = new Vector3();
		_velocity = new Vector3();
	}
	
	// Update is called once per frame
	void Update () {
		if( _isDying ) return; 

		//Here we check for certain player input.
		//	If they press the left arrow, they will rotate around the Z axis.
		//	If they press the right arrow, we rotate them the opposite direction around the Z.
		// Left/Right arrows rotate turret
		if (Input.GetKey ( keyRotateCounterClockwise )) {
			//Each game object in Unity has a transform. This keeps track of the
			//	position, rotation and scale of that object. The transform has its
			//	own functions that the programmer can use to manipulate it, such as
			//	Rotate(x, y, z).
			transform.Rotate ( 0, 0, Time.deltaTime * playerRotationSpeed );
			//deltaTime is the speed at which the game is running.
			//We multiply the playerSpeed by deltaTime so that the player will move at
			//	a consistent speed, no matter how fast the game is running.
		} else if (Input.GetKey ( keyRotateClockwise ) ) {
			transform.Rotate ( 0, 0, Time.deltaTime * -playerRotationSpeed );
		}

		// WASD for directional movement
		if( allowMovement )
		{
			_acceleration = Vector2.zero;

			if( Input.GetKey( keyMoveLeft ) )
			{
				_acceleration += Vector3.left;
				// tTrialPosition = new Vector2( tTrialPosition.x - movementSpeed, tTrialPosition.y );
			} else if( Input.GetKey( keyMoveRight ) )
			{
				_acceleration += Vector3.right;
//				tTrialPosition = new Vector2( tTrialPosition.x + movementSpeed, tTrialPosition.y );
			};

			if( Input.GetKey( keyMoveUp ) )
			{
				_acceleration += Vector3.up;
//				tTrialPosition = new Vector2( tTrialPosition.x, tTrialPosition.y + movementSpeed );
			} else if( Input.GetKey( keyMoveDown ) )
			{
				_acceleration += Vector3.down;
//				tTrialPosition = new Vector2( tTrialPosition.x, tTrialPosition.y - movementSpeed );
			}

			_acceleration *= movementSpeed * 0.0001f;
			_velocity += _acceleration;

			Vector3 tTrialPosition = transform.position + _velocity;

			if( !_gameManager.IsInBounds( tTrialPosition ) )
			{
				if( _gameManager.asteroidsBoundaries )
				{
					if( tTrialPosition.x > _gameManager.maxX ) {
						tTrialPosition.x = _gameManager.minX;
					} else if( tTrialPosition.x < _gameManager.minX ) {
						tTrialPosition.x = _gameManager.maxX;
					} else if( tTrialPosition.y > _gameManager.maxY ) {
						tTrialPosition.y = _gameManager.minY;
					} else if( tTrialPosition.y < _gameManager.minY ) {
						tTrialPosition.y = _gameManager.maxY;
					}
				}
				else
				{
					// prevent going too far out of bounds. 
					// Reset tTrialPosition to current position so that when tTrialPosition is used below 
					// it will not move the position of the Player
					tTrialPosition = transform.position;
				}
			}

			// now set the new position to the trial position
			transform.position = tTrialPosition;

			_velocity *= 1f - _physicsDrag;
		}

		//If the player hits space, we will instantiate(create and place) a bullet prefab.
		// Input.GetKey (KeyCode.Space) removed need to press space bar to fire. Now we always fire! 
		if (Time.time - _lastProjectileShotTime > _weaponScript.projectileFireDelaySeconds ) 
		{
			// if continuousFiring was set to true, fire. 
			// or, if user is pressing the fire button
			if( continuousFiring == true || Input.GetKey ( keyFire ) )
			{
				// Instaniate the bullet at the player's position and rotation
				// use bulletLaunchOffset to adjust the bullet position to be at the end of the turret 
				Vector2 tForward = transform.right;
				tForward.Normalize();
				Vector2 tBulletPosition = transform.position;
				tBulletPosition.x += tForward.x * _weaponScript.projectileOffset;
				tBulletPosition.y += tForward.y * _weaponScript.projectileOffset;

				GameObject.Instantiate(weapon, tBulletPosition, transform.rotation);

				ResetBulletDelayTimer();
			}
		}

	}

	//This function is called by the Unity engine when this object overlaps another.
	void OnCollisionEnter2D(Collision2D collidedWith) {
		if (collidedWith.gameObject.tag == "Enemy" && !_isDying) {
			// Decrease user health and update lives display
			playerHealth -= collidedWith.gameObject.GetComponent<Enemy>().impactDamage;
			playerHealth = Mathf.Max( 0, playerHealth );
			_livesDisplay.text = playerHealth.ToString();

			// Destroy the obstacle that collided with the player.
			Destroy(collidedWith.gameObject);

			// play death sound
			_audioInjury.Play();

			// If user has died, wait for sound to complete then go to Game Over screen
			if( playerHealth <= 0 )
			{
				_isDying = true;
				StartCoroutine( DoDeath() );
			}
		}
	}

	IEnumerator DoDeath()
	{
		GetComponent<BoxCollider2D>().enabled = false;

		// wait until the injury audio file finishes before we end the game
		yield return new WaitForSeconds( _audioInjury.clip.length );
		
		_sceneSwitcher.SwitchToSceneNumber( 2 );
	}

	void ResetBulletDelayTimer() {
		_lastProjectileShotTime = Time.time;
	}
}
