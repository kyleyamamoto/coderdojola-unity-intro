using UnityEngine;
using System.Collections;

public class WeaponSuperclass : MonoBehaviour 
{
	public int damageStrength = 1;
	public float projectileSpeed = 0.07f;
	public float projectileFireDelaySeconds = 0.3f;
	public float projectileOffset = 0f;
}

