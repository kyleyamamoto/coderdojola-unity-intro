using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	// There will only be one game manager in the game. Because of this,
	// we can set it up as a singleton. A singleton is a lone reference
	// of a class that all other classes are able to reference without
	// any setup. This means that you can call its functions anywhere!
	public static GameManager instance {get; private set;}

	[Tooltip("Use wraparound boundaries like in the game Asteroids")]
	public bool asteroidsBoundaries = false;

	[Tooltip("How many enemies are spawned per minute")]
	public float enemiesPerMinute = 60;

	// Pick the enemy prefab. The prefab must meet the enemy requirements (uses enemy script, tagged enemy, etc.). 
	// The easiest way to make a new enemy is to make a copy of the starter enemy and then make changes to your copy.
	[Tooltip("A list of the types of enemies to spawn. Repeats are allowed to spawn a higher proportion of that enemy than the others.")]
	public GameObject[] enemyPrefab = new GameObject[1];
	
	[HideInInspector]
	public float minX, maxX, minY, maxY;

	private float _padding, _spawnInterval, _distanceToOutOfBounds, _nextSpawnTime, _lastSpawnTime;
	private Canvas _canvas;
	private Model _model;
	private Text _scoreDisplay;

	void Awake() {
		instance = this;

		_canvas = GameObject.Find("GameSceneCanvas").GetComponent<Canvas>();
		_model = Model.instance;

		_scoreDisplay = _canvas.transform.Find( "Score Value" ).GetComponent<Text>(); 
		_padding = 0.5f;
		_spawnInterval = 60f / enemiesPerMinute;

		_model.SetScore( 0 );

		ResetTimer();
	}


	void Start() {
		//We're going to calculate the world bounds of the 2D screen, so that
		//	we can do things like delete objects that go outside the
		//	bounds. That way we won't have a bunch of objects that we
		//	can't see slowing down our game!

		//Screen.width and Screen.height give pixel values, but we want to get
		//	the position values in Unity's world space, which are different.
		//First, we can get the vertical dimension(height) of the screen
		float tHeight = Camera.main.GetComponent<Camera>().orthographicSize;
		//We can calculate our horizontal dimension(width), by multiplying
		//	the height by our screen's aspect ratio(width divided by height).
		float tWidth = tHeight * Screen.width/Screen.height;

		minX = -tWidth - _padding;
		maxX = tWidth + _padding;
		minY = -tHeight - _padding;
		maxY = tHeight + _padding;

		// _distanceToOutOfBounds is distance from center of screen to a corner
		_distanceToOutOfBounds = Mathf.Sqrt( tHeight * tHeight + tWidth * tWidth );

		_scoreDisplay.text = _model.GetScore().ToString();

		ResetTimer();
	}

	void Update() {
		if(Time.time > _nextSpawnTime) {
			int tRandomEnemy = Mathf.FloorToInt ( Random.Range( 0, enemyPrefab.Length ) );
			GameObject.Instantiate( enemyPrefab[ tRandomEnemy ], GetRandomPosition(), transform.rotation);
			ResetTimer();
		}
	}

	public void UpdateScoreDisplay() {
		_scoreDisplay.text = _model.GetScore().ToString();
	}
	
	// Anything can call this to see if the position is within the game boundaries
	public bool IsInBounds(Vector2 pPosition) {
		//if the object has left the game bounds, we will return false
		if(pPosition.x > maxX || pPosition.x < minX || pPosition.y > maxY || pPosition.y < minY) {
			return false;
		} else {
			return true;
		}
	}
	
	private void ResetTimer() {
		_lastSpawnTime = Time.time;
		_nextSpawnTime = _lastSpawnTime + _spawnInterval;
	}

	private Vector2 GetRandomPosition() {
		float tDirectionRadians = Random.Range ( 0, 360f ) * Mathf.Rad2Deg;
		float tX = Mathf.Cos (tDirectionRadians) * _distanceToOutOfBounds;
		float tY = Mathf.Sin (tDirectionRadians) * _distanceToOutOfBounds;

		return new Vector2( tX, tY );
	}
}
