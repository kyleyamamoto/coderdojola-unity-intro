using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	[Tooltip("Lives")]
	public int health = 3;
	[Tooltip("Minimum Speed")]
	public float minSpeed = 0.01f;
	[Tooltip("Maximum Speed")]
	public float maxSpeed = 0.03f;
	[Tooltip("Animation to play after enemy has been destroyed")]
	public GameObject deathAnimation;
	[Tooltip("Points for destroying enemy")]
	public int destructionValue = 1;
	[Tooltip("Damage done to Player if enemy touches it")]
	public int impactDamage = 1;
	[Tooltip("How much to push the enemy backwards when struck by a projectile")]
	public float stepBackwardsAmount = 0.1f;


	//	private variables are hidden in the Unity inspector because you can only change them from the other code in the same script.
	private Vector2 _directionVector;
	private Vector3 _playerPosition;
	private AudioSource _audioSource;
	private bool _isDying = false;
	private float _angle, _startTime;
	private GameObject _player;
	private float _speed;

	// Start runs once when the enemy is created
	void Start () {
		_audioSource = GetComponent<AudioSource>();

		// set the speed and direction of our enemy	
		SetRandomSpeed ();

		_player = GameObject.Find( "Player" );

		// face enemy towards Player
		SetAngle();	

		// record the time the enemy was created 
		_startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		// Carcasses of enemies shouldn't move while dying, so prevent the rest of the Update 
		// method from being run if _isDying is true
		if( _isDying == true ) {
			return;
		}

		// without constatnt re-straightening the enemies can get pushed into an odd angle. 
		// rotate to face Player every frame
		SetAngle(); 

		// To move the obstacle, we first get it's current position from the transform.
		Vector2 nextPosition = transform.position;
		// Then, we add the direction vector to that position. _directionVector is set when SetAngle is run, above.
		nextPosition.x += _directionVector.x * _speed;
		nextPosition.y += _directionVector.y * _speed;
		// Finally, we assign the position to the next position we have created.
		transform.position = nextPosition;

		// Check if the enemy is still in the bounds of the game.
		bool inBounds = GameManager.instance.IsInBounds(transform.position);

		// If it isn't, delete it.
		// Also require an enemy to be alive for more than 4 seconds before it can wander off the screen and die.  
		if( inBounds == false && Time.time > _startTime + 4f ) {
			Destroy(this.gameObject);
		}
	}

	// This function is called by the Unity engine when this object overlaps another.
	void OnCollisionEnter2D( Collision2D collidedWith ) {
		// Objects can be tagged in the Unity Inspector so that they can
		// easily be found in other parts of code. In our game we tag the Game Object that holds our projectiles as "Weapon"
		// When an enemy touches a GameObject with the parent (container) GameObject tagged "Weapon", we know that
		// a projectile has hit the enemy.
		if( collidedWith.gameObject.transform.parent != null && collidedWith.gameObject.transform.parent.gameObject.tag == "Weapon" ) 
		{
			// When an enemy is struck by a projetile, the enemy is pushed back by the amount set 
			StepBackwards();

			int tDamage = collidedWith.transform.parent.gameObject.GetComponent<WeaponSuperclass>().damageStrength;
		
			// Destroy the bullet the obstacle collided with.
			// Destroy(collidedWith.gameObject);
			// setActive false instead so that the other bullet(s) can remain active. 
			// the bullet container will be destroyed when it leaves the scene. 
			collidedWith.gameObject.SetActive( false );
			// Lower health by the projectile's damage strength if it overlapped the enemy.
			StartCoroutine( DecreaseHealth( tDamage ) );
		}
	}

	//This function will randomly choose a speed for the enemy.
	void SetRandomSpeed() {
		// Random.range is a pre-existing function that will pick a number somewhere between
		//	the numbers we give it. The function may also pick the lower or upper bound we
		//	give it. Including the high and low values as possible return values is called being inclusive.
		_speed = Random.Range( minSpeed, maxSpeed );
	}

	// Method that will check if the object dies when health decreases
	// This method uses a yield statement, so it must return IEnumerator
	IEnumerator DecreaseHealth( int pDamage ) {
		health -= pDamage;
		if (health <= 0 && !_isDying) {
			// prevent multiple calls to this while the sprite is still active for audio purposes, but invisible. 
			_isDying = true;
			GetComponent<BoxCollider2D>().enabled = false;

			// increment score
			Model.instance.ChangeScore( destructionValue );
			GameManager.instance.UpdateScoreDisplay();

			// play death sound
			_audioSource.Play();

			// make enemy sprite component invisible by setting alpha to zero. 
			// Don't set active false b/c we still want to play sounds
			GetComponent<SpriteRenderer>().color = new Color( 0, 0, 0, 0 );

			// example code for Instantiating a Game Object from the Resources folder using Resources.Load()
			// GameObject tExplosion = Instantiate( Resources.Load( "explosion" , typeof(GameObject) ) ) as GameObject;

			// instantiate a copy of the Death Animation Game Object
			GameObject tDeathAnim = Instantiate( deathAnimation, transform.position, transform.rotation ) as GameObject;

			// set the death animation's parent to the enemy so that when the enemy is destroyed in a moment 
			// the explosion will also be destroyed. 
			tDeathAnim.transform.parent = gameObject.transform;
		
			// wait for audio clip to finish before destroying this enemy
			yield return new WaitForSeconds( _audioSource.clip.length );

			// if the object is out of health, remove it from the game
			Destroy(this.gameObject);
		}
	}

	void StepBackwards() {
		Vector2 tPosition = transform.position;
		tPosition.x += _directionVector.x * -stepBackwardsAmount;
		tPosition.y += _directionVector.y * -stepBackwardsAmount;
		transform.position = tPosition;
	}

	void SetAngle()
	{
		_playerPosition = _player.transform.position;
		
		_directionVector = _playerPosition - transform.position;
		_directionVector.Normalize();
		
		_angle = Mathf.Atan2(_directionVector.y,_directionVector.x) * Mathf.Rad2Deg;

		transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
	}

}
